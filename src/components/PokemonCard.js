import styled from "styled-components";
import React, { Component } from "react";
import { Column, Row } from "./Navbar";
import {Link} from "@reach/router";

const CardImageBorder = styled.div`
  margin: 1rem 2rem 0 2rem;
  border: solid black 2px;
  border-top-left-radius: 3vw;
  border-top-right-radius: 3vw;
  height: 20vh;
  width: 15vw;
  background-image: linear-gradient(
    90deg,
    ${props => (props.colors ? props.colors : "gray, gray")}
  );
`;
const CardInfoBorder = styled.div`
  margin-bottom: 2rem;
  border: solid black 2px;
  height: 10vh;
  width: 15vw;
  background-color: lightgray;
`;

const Image = styled.img`
  height: 20vh;
  width: 15vw;
`;

// https://stackoverflow.com/questions/48713421/target-child-element-styled-components
const ClickableArea = styled.div`
  &:hover {
    cursor: pointer;
    ${CardImageBorder} {
      height: 22vh;
      width: 17vw;
    }

    ${CardInfoBorder} {
      width: 17vw;
    }

    ${Image} {
      transition-duration: 2500ms;
      transition-property: transform;
      transition-timing-function: ease-out;
      transform: scale(1.5, 1.5);
      z-index: 2;
    }
  }
`;

const ClickHandler = styled(Link)`
  text-decoration: none;
  color: #000;
  &:visited{
    color: #000;
  }
`;

export class PokemonCard extends Component {
  render() {
    let colors = defineColors(this.props.kind);
    let url;

    if(this.props.user!=null){
      url=`/inspect/${this.props.user}/${this.props.name}`;
    }else{
      url=`/inspect/${this.props.name}`;
    }

    return (
      <>
        <ClickableArea>
          <ClickHandler to={url}>
          <Column>
            <CardImageBorder colors={colors}>
              <Image src={this.props.image_url}></Image>
            </CardImageBorder>
            <CardInfoBorder>
              <Column>
                <Row>
                  <span>Nome:</span>
                  <span>
                    {this.props.name.charAt(0).toUpperCase() +
                      this.props.name.slice(1)}
                  </span>
                </Row>
                <Row>
                  <span>Tipo:</span>
                  <span>{this.props.kind}</span>
                </Row>
              </Column>
            </CardInfoBorder>
          </Column>
          </ClickHandler>
        </ClickableArea>
      </>
    );
  }
}

function defineColors(kind) {
  let colors = "";
  let regEx2 = /^(.*)[;](.*)$/; // não pode ter "" só deus sabe pq
  let regEx1 = /^(.*)$/;
  let matchArr;

  if (kind.match(";")) {
    matchArr = kind.match(regEx2);
  } else {
    matchArr = kind.match(regEx1);
  }
  if (matchArr.length === 2) {
    matchArr.push(matchArr[1]);
  }
  switch (matchArr[1]) {
    case "normal":
      colors = colors + "#A8A878";
      break;
    case "fighting":
      colors = colors + "#C02F28";
      break;
    case "flying":
      colors = colors + "#A790F0";
      break;
    case "poison":
      colors = colors + "#A03FA0";
      break;
    case "ground":
      colors = colors + "#E0BF68";
      break;
    case "rock":
      colors = colors + "#B8A038";
      break;
    case "bug":
      colors = colors + "#A8B81F";
      break;
    case "ghost":
      colors = colors + "#705798";
      break;
    case "steel":
      colors = colors + "#B8B8D0";
      break;
    case "fire":
      colors = colors + "#F08030";
      break;
    case "water":
      colors = colors + "#6890F0";
      break;
    case "grass":
      colors = colors + "#78C850";
      break;
    case "electric":
      colors = colors + "#F8CF2F";
      break;
    case "psychic":
      colors = colors + "#F85887";
      break;
    case "ice":
      colors = colors + "#97D8D8";
      break;
    case "dragon":
      colors = colors + "#7037F8";
      break;
    case "dark":
      colors = colors + "#705847";
      break;
    case "fairy":
      colors = colors + "#EE99AC";
      break;
    default:
      colors = colors + "67A090"; // ???
  }

  colors = colors + ", ";

  switch (matchArr[2]) {
    case "normal":
      colors = colors + "#A8A878";
      break;
    case "fighting":
      colors = colors + "#C02F28";
      break;
    case "flying":
      colors = colors + "#A790F0";
      break;
    case "poison":
      colors = colors + "#A03FA0";
      break;
    case "ground":
      colors = colors + "#E0BF68";
      break;
    case "rock":
      colors = colors + "#B8A038";
      break;
    case "bug":
      colors = colors + "#A8B81F";
      break;
    case "ghost":
      colors = colors + "#705798";
      break;
    case "steel":
      colors = colors + "#B8B8D0";
      break;
    case "fire":
      colors = colors + "#F08030";
      break;
    case "water":
      colors = colors + "#6890F0";
      break;
    case "grass":
      colors = colors + "#78C850";
      break;
    case "electric":
      colors = colors + "#F8CF2F";
      break;
    case "psychic":
      colors = colors + "#F85887";
      break;
    case "ice":
      colors = colors + "#97D8D8";
      break;
    case "dragon":
      colors = colors + "#7037F8";
      break;
    case "dark":
      colors = colors + "#705847";
      break;
    case "fairy":
      colors = colors + "#EE99AC";
      break;
    default:
      colors = colors + "67A090"; // ???
  }

  return colors;
}

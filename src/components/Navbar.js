import React, { Component } from "react";
import styled from "styled-components";
import { Link } from "@reach/router";

const NavbarBackground = styled.header`
  z-index: 3;
  height: 18vh;
  width: 100vw;
  background-image: linear-gradient(45deg, #0DE6FF, #0CE81D);
  position: fixed;
  top: 0;
  left: 0;
  border-bottom-left-radius: 2vw;
  border-bottom-right-radius: 2vw;
  box-shadow: 5px 5px 3px rgba(0,0,0,0.3);
  /* background-color: ${props => (props.color ? props.color : "red")}; */
`;

export const Row = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: ${props =>
    props.justify ? props.justify : "space-between"};
  align-items: center;
  height: ${props => (props.height ? props.height : "")};
  width: 100%;
`;

export const Column = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: space-between;
  align-items: center;
  width: ${props => (props.width ? props.width : "100%")};
`;

const Title = styled.h1`
  color: #e88c0c;
  text-align: center;
  font-size: 6rem;
  font-family: cursive;
  margin-top: 0.2em;
`;

const StyledLink = styled(Link)`
  text-decoration: none;
  color: #fff200;
  font-size: 1rem;
  font-weight: bold;
  padding: 0 2em;
  transition-duration: 400ms;

  &:hover {
    background-color: snow;
    border-top-right-radius: 100vw;
    border-top-left-radius: 100vw;
    font-size: 1.5rem;
    padding: 1em 2em;
  }
`;

export class Navbar extends Component {
  render() {
    let profile;
    let loginButton;
    let home;
    if (this.props.user) {
      profile = (
        <StyledLink to={`/users/${this.props.user}`}>
          Ver perfil de {this.props.user}
        </StyledLink>
      );
      loginButton = <StyledLink to="/">Logout</StyledLink>;
      home = <StyledLink to={`/${this.props.user}`}>Home</StyledLink>
    } else {
      loginButton = <StyledLink to="/login">Login</StyledLink>;
      home = <StyledLink to="/">Home</StyledLink>;
    }
    return (
      <>
        <NavbarBackground>
          <Row height="80%" justify="center">
            <Title>Pokedex</Title>
          </Row>
          <Row height="20%">
            <Column width="45%">
              <Row>
                {home}
                {profile}
              </Row>
            </Column>
            {loginButton}
          </Row>
        </NavbarBackground>
      </>
    );
  }
}

export default Navbar;

import styled from "styled-components";

const BackgroundArea = styled.section`
    margin-top: 2rem;
    min-height: 80vh;
    width: 90vw;
    background-color: white;
    border-top-left-radius: 5vw;
    border-top-right-radius: 5vw;
    border-bottom-left-radius: 5vw;
    border-bottom-right-radius: 5vw;
    display: flex;
`;

export default BackgroundArea;
import styled from "styled-components";

export const Title = styled.h1`
  font-family: "Segoe UI", Tahoma, Geneva, Verdana, sans-serif;
  font-size: 1.5rem;
  letter-spacing: 0.2rem;
  text-align: center;
`;

export const UserInput = styled.input`
    font-size: 1.5rem;
    height: 2rem;
    margin: 1rem 0;
    text-align: center;
`;

export const TextBody = styled.p`
    margin: 0 12rem;
    text-align: center;
    font-size:0.8rem;
`;

export default Title;

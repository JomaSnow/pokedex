import styled from "styled-components";
import  React, {Component } from "react";
import { Row } from "./Navbar";

// https://www.w3schools.com/howto/howto_css_arrows.asp

const PreviousPage = styled.div`
  opacity: inherit;
  border: solid black;
  border-width: 0 ${props => (props.size ? props.size : "3px")}
    ${props => (props.size ? props.size : "3px")} 0;
  display: inline-block;
  padding: ${props => (props.size ? props.size : "3px")};
  transform: rotate(135deg);
  -webkit-transform: rotate(135deg);
  justify-content: center;
`;

const NextPage = styled.div`
  border: solid black;
  border-width: 0 ${props => (props.size ? props.size : "3px")}
    ${props => (props.size ? props.size : "3px")} 0;
  display: inline-block;
  padding: ${props => (props.size ? props.size : "3px")};
  transform: rotate(-45deg);
  -webkit-transform: rotate(-45deg);
`;

const CurrentPageText = styled.span`
  font-size: 0.8rem;
  display: none;
`;

// const CurrentPageNumber = styled.span``;

const SelectorArea = styled.section`
  position: fixed;
  bottom: 2vh;
  height: 1rem;
  width: 9vw;
  background-color: #ff0b00;
  opacity: 40%;
  z-index: 2;
  border: 1px black solid;
  border-top-left-radius: 25px;
  border-top-right-radius: 25px;

  &:hover {
    opacity: 100%;
    height: 3rem;
    width: 27vw;

    ${PreviousPage} {
      border-width: 0 10px 10px 0;
      padding: 10px;
      padding: 10px;
      cursor: pointer;
    }

    ${NextPage} {
      border-width: 0 10px 10px 0;
      padding: 10px;
      padding: 10px;
      cursor: pointer;
    }

    ${CurrentPageText} {
      display: inline;
    }
  }
`;

// https://stackoverflow.com/questions/48407785/react-pass-function-to-child-component

export class PageSelector extends Component {
  render() {
    return (
      <>
        <SelectorArea>
          <Row justify="space-around" height="100%">
            <PreviousPage
              size="2px"
              onClick={this.props.prev}
            ></PreviousPage>
            <div>
              <CurrentPageText>página: </CurrentPageText>
              {this.props.currentPage}
              <CurrentPageText> de 33</CurrentPageText>
            </div>
            <NextPage
              size="2px"
              onClick={this.props.next}
            ></NextPage>
          </Row>
        </SelectorArea>
      </>
    );
  }
}

export default PageSelector;

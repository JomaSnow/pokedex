import React, { Component } from "react";
import { Navbar, Row, Column } from "./../components/Navbar";
import { PokemonCard } from "./../components/PokemonCard";
import Container from "./../components/Container";
import BackgroundArea from "./../components/BackgroundArea";
import { Title, TextBody } from "./../components/Title";
import axios from "axios";

class Profile extends Component {
  state = {
    user: null
  };

  componentDidMount() {
    axios
      .get("https://pokedex-cjr.herokuapp.com/users/" + this.props.username)
      .then(res => {
        this.setState({ user: res.data });
      })
      .catch(e => {
        alert("Este usuario nao existe." + e);
      });
  }

  render() {
    console.log(this.state.user);
    if (!this.state.user) {
      return (
        <>
          <Navbar></Navbar>
          <Container>
            <h1>Carregando...</h1>
          </Container>
        </>
      );
    } else {
      let rows = [],
        cols = [];
      let index = 0;
      const totalCols = this.state.user.pokemons.length;

      for (index; index < totalCols; index++) {
        cols.push(
          <PokemonCard
            key={index}
            name={this.state.user.pokemons[index].name}
            kind={this.state.user.pokemons[index].kind}
            image_url={this.state.user.pokemons[index].image_url}
            user={this.props.username}
          ></PokemonCard>
        );

        if ((index + 1) % 4 === 0 || index + 1 === totalCols) {
          rows.push(<Row key={index}>{cols}</Row>);
          cols = [];
        }
      }
      return (
        <>
          <Navbar user={this.props.username}></Navbar>
          <Container>
            <BackgroundArea>
              <Column>
                <Row>
                  <Column>
                    <Title>Perfil de {this.props.username}</Title>
                    <br />
                    <TextBody>
                      Este é o seu perfil. Aqui você pode ver todos os seus
                      Pokemon favoritos e administrá-los como quiser.
                    </TextBody>
                  </Column>
                </Row>
                <Row justify="center">
                  <span>Número de Pokemon favoritos: </span>
                  {this.state.user.pokemons.length}
                </Row>
                {rows}
              </Column>
            </BackgroundArea>
          </Container>
        </>
      );
    }
  }
}

export default Profile;

import React, { Component } from "react";
import { Navbar, Column, Row } from "./../components/Navbar";
import Container from "./../components/Container";
import axios from "axios";
import { Redirect } from "@reach/router";
import { Title, UserInput, TextBody } from "./../components/Title";

class Login extends Component {
  state = {
    username: "",
    canRedirect: false
  };

  setUsername = event => {
    this.setState({ username: event.target.value });
  };

  submitForm = event => {
    event.preventDefault();
    axios
      .get("https://pokedex-cjr.herokuapp.com/users/" + this.state.username)
      .then(res => {
        console.log("user encontrado. " + res);
      })
      .catch(err => {
        console.log("nenhum usuario encontrado. Criando novo usuario. " + err);
        axios
          .post("https://pokedex-cjr.herokuapp.com/users", {
            username: this.state.username
          })
          .then(res => {
            console.log("criou user. " + res);
          })
          .catch(e => {
            alert("Algo deu errado! \n" + e);
          });
      });
    this.setState({ canRedirect: true });
  };

  render() {
    console.log(this.state.username);

    if (this.state.canRedirect) {
      return <Redirect to={`/${this.state.username}`}></Redirect>;
    } else {
      return (
        <>
          <Navbar></Navbar>
          <Container>
            <Row height="30vh">
              <Column>
                <Title>Login</Title>
                <br />
                <TextBody>
                  Entre com um usuário existente ou informe um novo username
                  para criar um novo usuário.
                </TextBody>
                <br />
                <form onSubmit={this.submitForm}>
                  <UserInput
                    type="text"
                    value={this.state.username}
                    onChange={this.setUsername}
                    placeholder="Username"
                  ></UserInput>
                </form>
              </Column>
            </Row>
          </Container>
        </>
      );
    }
  }
}

export default Login;

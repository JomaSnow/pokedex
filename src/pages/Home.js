import React, { Component } from "react";
import { Navbar, Row, Column } from "./../components/Navbar";
import Container from "./../components/Container";
import BackgroundArea from "./../components/BackgroundArea";
import PageSelector from "./../components/PageSelector";
import { PokemonCard } from "./../components/PokemonCard";
import axios from "axios";

class Home extends Component {
  state = {
    pokemons: [],
    currentPage: 1,
    nextPage: null,
    prevPage: null,
    user: null,
    loading: true
  };

  updatePokemonList() {
    axios
      .get(
        `https://pokedex-cjr.herokuapp.com/pokemons?page=${this.state.currentPage}`
      )
      .then(res => {
        const pokemons = res.data.data;
        const nextPage = res.data.next_page;
        const prevPage = res.data.prev_page;
        this.setState({ pokemons, nextPage, prevPage, loading: false });
      });
  }

  componentDidMount() {
    this.updatePokemonList();
  }

  // precisa do async await senao ele nao atualiza o componente (precisa clicar duas vezes)
  goToNextPage = async () => {
    await this.setState({ loading: true });
    await this.setState({ currentPage: this.state.nextPage });
    this.updatePokemonList();
  };

  goToPrevPage = async () => {
    await this.setState({ loading: true });
    await this.setState({ currentPage: this.state.prevPage });
    this.updatePokemonList();
  };

  // https://stackoverflow.com/questions/36318601/react-js-every-nth-item-add-opening-tag-or-closing-tag

  render() {
    let rows = [],
      cols = [];
    let index = 0;
    const totalCols = this.state.pokemons.length;

    let user;

    // potencialmente codigo morto
    if (this.props.username) {
      user = this.props.username;
    }

    for (index; index < totalCols; index++) {
      cols.push(
        <PokemonCard
          key={index}
          name={this.state.pokemons[index].name}
          kind={this.state.pokemons[index].kind}
          image_url={this.state.pokemons[index].image_url}
          user={user}
        ></PokemonCard>
      );

      if ((index + 1) % 4 === 0 || index + 1 === totalCols) {
        rows.push(<Row key={index}>{cols}</Row>);
        cols = [];
      }
    }

    if (this.state.currentPage === null) {
      this.setState({ currentPage: 1 });
    }

    if (this.state.prevPage === null) {
      this.setState({ prevPage: 33 });
    }

    if (this.state.loading) {
      return (
        <>
          <Navbar user={user}></Navbar>
          <Container>
            <BackgroundArea>
              <Column>
                <h1>Carregando Pokemons...</h1>
                <PageSelector
                  currentPage={this.state.currentPage}
                  prev={this.goToPrevPage}
                  next={this.goToNextPage}
                ></PageSelector>
              </Column>
            </BackgroundArea>
          </Container>
        </>
      );
    } else {
      return (
        <>
          <Navbar user={user}></Navbar>
          <Container>
            <BackgroundArea>
              <Column>
                {rows}
                <PageSelector
                  currentPage={this.state.currentPage}
                  prev={this.goToPrevPage}
                  next={this.goToNextPage}
                ></PageSelector>
              </Column>
            </BackgroundArea>
          </Container>
        </>
      );
    }
  }
}
export default Home;

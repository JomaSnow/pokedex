import React, { Component } from "react";
import { Navbar, Column, Row } from "./../components/Navbar";
import BackgroundArea from "./../components/BackgroundArea";
import Container from "./../components/Container";
import { Title } from "./../components/Title";
import axios from "axios";
import styled from "styled-components";

const Portrait = styled.img`
  height: 50vh;
  width: 50vw;
  border: 1px solid black;
  box-shadow: 3px 3px 3px grey;
  background-color: lightslategray;
  margin: 0 0 0 2rem;
`;

const Label = styled.span`
  color: red;
  justify-content: justify;
  margin: 0 1rem;
`;

const FavButton = styled.button`
  display: ${props => (props.hasUser ? "block" : "none")};
  height: 2rem;
  min-width: 10rem;
  font-size: 1.1rem;
  font-weight: bold;
  background-color: lightpink;
`;

class Pokemon extends Component {
  state = {
    pokemon: null,
    isFav: false,
    allPokemon: []
  };

  componentDidMount() {
    axios
      .get("https://pokedex-cjr.herokuapp.com/pokemons/" + this.props.pokemon)
      .then(res => {
        this.setState({ pokemon: res.data });
        if (this.props.username != null) {
          axios
            .get(
              "https://pokedex-cjr.herokuapp.com/users/" + this.props.username
            )
            .then(response => {
              this.setState({ allPokemon: response.data.pokemons });
              if (this.state.allPokemon.length > 0) {
                for (let i = 0; i < this.state.allPokemon.length; i++) {
                  if (this.state.pokemon.id === this.state.allPokemon[i].id) {
                    this.setState({ isFav: true });
                  }
                }
              }
            });
        }
      })
      .catch(e => {
        alert("Algo deu errado! " + e);
      });
  }

  addToFavorite = async () => {
    await axios
      .post(
        `https://pokedex-cjr.herokuapp.com/users/${this.props.username}/starred/${this.props.pokemon}`
      )
      .then(res => {
        this.setState({ isFav: true });
      })
      .catch(er => {
        alert("Algo deu errado! " + er);
      });
  };

  removeFromFavorite = async () => {
    await axios
      .delete(
        `https://pokedex-cjr.herokuapp.com/users/${this.props.username}/starred/${this.props.pokemon}`
      )
      .then(res => {
        this.setState({ isFav: false });
      })
      .catch(er => {
        alert("Algo deu errado! " + er);
      });
  };

  render() {
    if (this.state.pokemon === null) {
      return (
        <>
          <Navbar user={this.props.username}></Navbar>
          <Container>
            <BackgroundArea>
              <Column>
                <h1>Carrregando...</h1>
              </Column>
            </BackgroundArea>
          </Container>
        </>
      );
    } else {
      let button;

      if (this.props.username != null) {
        if (!this.state.isFav) {
          button = (
            <FavButton onClick={this.addToFavorite} hasUser={true}>Adicionar aos Favoritos</FavButton>
          );
        } else {
          button = <FavButton onClick={this.removeFromFavorite} hasUser={true}>Remover dos Favoritos</FavButton>;
        }
      }

      return (
        <>
          <Navbar user={this.props.username}></Navbar>
          <Container>
            <BackgroundArea>
              <Column>
                <Row justify="space-around" height="10vh">
                  <Title>{this.state.pokemon.name}</Title>
                </Row>
                <Row justify="space-around">
                  <Portrait src={this.state.pokemon.image_url}></Portrait>
                  <Column>
                    <Row justify="center">
                      <Label>Id: </Label>
                      <span>#00{this.state.pokemon.id}</span>
                    </Row>
                    <Row justify="center">
                      <Label>Nome: </Label>
                      <span>{this.state.pokemon.name}</span>
                    </Row>
                    <Row justify="center">
                      <Label>Peso: </Label>
                      <span>{this.state.pokemon.weight / 10} kg</span>
                    </Row>
                    <Row justify="center">
                      <Label>Tamanho: </Label>
                      <span>{this.state.pokemon.height * 10} cm</span>
                    </Row>
                    <Row justify="center">
                      <Label>Tipo: </Label>
                      <span>{this.state.pokemon.kind}</span>
                    </Row>
                    <Row justify="center">{button}</Row>
                  </Column>
                </Row>
                <Row height="10vh"></Row>
              </Column>
            </BackgroundArea>
          </Container>
        </>
      );
    }
  }
}

export default Pokemon;

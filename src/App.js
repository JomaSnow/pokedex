import React from "react";
import "./App.css";
import { Router } from "@reach/router";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Profile from "./pages/Profile";
import Pokemon from "./pages/Pokemon";

// paleta de cores: 
// #0DE6FF  primaria
// #0CE81D
// #FFF200  
// #E88C0C
// #FF0B00

function App() {
  return <>
    <Router>
      <Home path="/"></Home>
      <Home path="/:username"></Home>
      <Login path="/login"></Login>
      <Profile path="/users/:username"></Profile>
      <Pokemon path="/inspect/:pokemon"></Pokemon>
      <Pokemon path="/inspect/:username/:pokemon"></Pokemon>
    </Router>
  </>;
}

export default App;
